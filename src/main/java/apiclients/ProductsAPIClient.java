package apiclients;

import io.restassured.RestAssured;
import models.Message;
import models.Product;

import java.util.Arrays;
import java.util.List;

public class ProductsAPIClient {
    private static final String URL = "https://waarkoop-server.herokuapp.com/api/v1/search/test/";

    public List<Product> getProducts(String validProductName) {
        return Arrays.asList(RestAssured.given().when()
                .get(URL + validProductName)
                .body()
                .as(Product[].class));

    }

    public Message getInvalidProducts(String invalidProductName) {
        return RestAssured.given().when()
                .get(URL + invalidProductName)
                .body()
                .as(Message.class);

    }
}
