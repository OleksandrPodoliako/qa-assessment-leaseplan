package utils;

import models.Message;
import models.Product;

import java.util.List;

public class SharedData {

    private static SharedData sharedData = null;

    private List<Product> products;
    private Message message;

    private SharedData() {
    }

    public static SharedData getSharedData() {
        if (sharedData == null) {
            sharedData = new SharedData();
        }
        return sharedData;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
