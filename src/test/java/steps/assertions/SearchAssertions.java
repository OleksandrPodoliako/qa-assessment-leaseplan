package steps.assertions;


import io.cucumber.java.en.Then;
import models.Message;
import models.Product;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static utils.SharedData.getSharedData;

public class SearchAssertions {
    @Then("^he sees the results displayed for (.+)")
    public void checkResultsForValidProduct(String productName) {
        List<Product> products = getSharedData().getProducts();

        assertThat(products.stream().anyMatch(pr -> pr.getTitle().toLowerCase().contains(productName)))
                .as(String.format("The product list should not be empty for %s", productName))
                .isTrue();
    }

    @Then("he does not see the results")
    public void callEndpoint() {
        Message message = getSharedData().getMessage();

        assertThat(message.getDetail().error)
                .as(String.format("The message type should be error for product %s", message.getDetail().requested_item))
                .isTrue();
    }
}
