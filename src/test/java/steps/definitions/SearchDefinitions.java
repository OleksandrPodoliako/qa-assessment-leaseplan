package steps.definitions;

import apiclients.ProductsAPIClient;
import io.cucumber.java.en.When;
import models.Message;
import models.Product;

import java.util.List;

import static utils.SharedData.getSharedData;

public class SearchDefinitions {
    @When("^he calls endpoint with valid product (.+)")
    public void callEndpoint(String product) {
        List<Product> products = new ProductsAPIClient().getProducts(product);
        getSharedData().setProducts(products);
    }

    @When("^he calls endpoint with invalid product (.+)")
    public void callEndpointInvalidProduct(String invalidProduct) {
        Message message = new ProductsAPIClient().getInvalidProducts(invalidProduct);
        getSharedData().setMessage(message);
    }
}
