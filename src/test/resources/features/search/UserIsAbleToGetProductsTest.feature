Feature: Search for the product

  Scenario Outline: Verify that user is able to get product list for valid product
    When he calls endpoint with valid product <product>
    Then he sees the results displayed for <product>

    Examples:
      | product |
      | apple   |
      | mango   |

  Scenario: Verify that user is not able to get product list for invalid product
    When he calls endpoint with invalid product car
    Then he does not see the results
